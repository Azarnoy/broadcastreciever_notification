package com.dmitriy.azarenko.broadcastreciever_notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;

/**
 * Created by Дмитрий on 11.02.2016.
 */
public class MyReciever extends BroadcastReceiver {

    private static final int NOTIFY_ID = 101;




    @Override
    public void onReceive(Context context, Intent intent) {
        Intent notificationIntent = new Intent(context,MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context,0,notificationIntent,PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification.Builder builder = new Notification.Builder(context);


        builder.setContentIntent(contentIntent)

                .setSmallIcon(R.drawable.eye)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.big_eye))

                .setTicker("Большой брат следит за тобой")
                .setContentTitle("Большой брат следит за тобой")
                .setContentText("Большой брат не дремлет");


        Notification n = builder.build();
        nm.notify(NOTIFY_ID, n);





    }


}
