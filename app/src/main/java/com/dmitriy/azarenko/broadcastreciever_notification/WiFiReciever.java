package com.dmitriy.azarenko.broadcastreciever_notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.widget.Toast;

public class WiFiReciever extends BroadcastReceiver {
    private static final int NOTIFY_ID = 102;
    long[] vibrate = new long[] { 1000, 500, 1000, 500, 1000 };


    @Override
    public void onReceive(Context context, Intent intent) {
        Intent notificationIntent = new Intent(context,MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context,0,notificationIntent,PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification.Builder builder = new Notification.Builder(context);


        builder.setContentIntent(contentIntent)

                .setSmallIcon(R.drawable.eye)
                .setVibrate(vibrate)
                .setSound(Uri.parse("file///sdcard/music/test"))

                .setTicker("Большой брат следит за твоим Wi-fi")
                .setContentTitle("Большой брат следит за твоим Wi-Fi")
                .setContentText("Большой брат не дремлет");




        Notification n = builder.build();
        nm.notify(NOTIFY_ID, n);

    }
}
